/*
r.js Optimizer Build Configuration File

The main things you'll probably need here:
    appDir --> The top level directory that contains your app.
    baseUrl --> all modules are located relative to this path (this is relative to "appDir")
    mainConfigFile --> the location of the main JS file from which to start optimization
    name --> The name of the module to start optimization from
    dir --> The directory path to save the optimized output
    optimizeCss --> Do CSS optimizations
    
See here for all configuration options:
https://github.com/requirejs/r.js/blob/master/build/example.build.js
*/
({
    appDir: "../dev/",
    baseUrl: "scripts",
    mainConfigFile: "../dev/scripts/main.js",
    name: "main",
    optimizeCss: "standard",
    dir: "../dist"
})
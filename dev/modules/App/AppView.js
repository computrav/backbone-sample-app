define([ "backbone", "backbone-epoxy", "../Person/PersonModel", "../Person/PersonCollection", "../Person/PersonFormView",  "../Person/PersonTableView", "text!./AppTemplate.html", "bootstrap" ], 
function (Backbone, Epoxy, PersonModel, PersonCollection, PersonFormView, PersonTableView, AppTemplate, Bootstrap) {
    var appView = Backbone.Epoxy.View.extend({
        initialize: function () {
            var self = this;

            this.template = _.template(AppTemplate);

            //We are rendering the main view before doing the async calls below, then they will attach their child views when they're ready
            this.render();

            //Load child views
            this.personFormView = this.loadPersonFormView();
            this.loadPersonTableView();
        },

        loadPersonFormView: function() {
            var self = this;

            //Instantiate view using the person as the model
            var personFormView = new PersonFormView({
                model: new PersonModel(),
                el: "div#personForm"
            });

            //Listen for an event that the person form has triggered a change (add/update/delete), that necessitates
            //the list of people to be refreshedrow click event in the table (which itself gets bubbled up from the individual row click)
            this.listenTo(personFormView, "PersonFormView:personChanged", function(e, view) {
                self.loadPersonTableView();
            });

            //Render child view
            personFormView.render();

            return personFormView;
        },

        loadPersonTableView: function() {
            var self = this;

            //Instantiate and fetch a collection of people
            var personCollection = new PersonCollection();
            var promise = personCollection.fetch();

            promise
                .done(function(data, textStatus, jqXHR) {
                    var personTableView = new PersonTableView({
                        el: self.$("div#personTable"),
                        collection: personCollection
                    });

                    //Listen for a row click event in the table (which itself gets bubbled up from the individual row click)
                    //Then load the person that was clicked on into the form
                    self.listenTo(personTableView, "PersonTable:rowClicked", function(e, view) {
                        self.personFormView.model.set("id", view.model.get("id"));
                        self.personFormView.model.fetch({
                            success: function() {
                                self.personFormView.render();
                            }
                        });
                    });

                    self.listenTo(personTableView, "PersonTable:rowRemoved", function(e, view) {
                        //If the person removed is currently loaded into the form, change form to "add" mode
                      
                        if (view.model.get("id") == self.personFormView.model.get("id")) {
                            self.personFormView.resetForm();
                        }
                      
                        //Re-load table
                        self.loadPersonTableView();
                    });

                    personTableView.render();
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    console.error("Error retrieving PersonCollection: " + jqXHR.status + " " + errorThrown);
                });
        },

        //Object for child views used by this parent view, to be able to access from other methods/callbacks
        //Not sure if we'll need this for now...
        // childViews: {},

        render: function() {
            this.$el.html(this.template());

            //Since we are setting HTML, the Epoxy bindings get removed, so we need to manually re-bind
            this.applyBindings();

            return this;
        }
    });

    return appView;
});
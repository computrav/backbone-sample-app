define([ "backbone", "backbone-epoxy", "appConfig" ],
function (Backbone, Epoxy, AppConfig) {
    var personModel = Backbone.Epoxy.Model.extend({
        defaults: {
            id: "",
            firstName: "",
            lastName: "",
            fullName: "",
            favoriteAnimal: ""
        },

        urlRoot: AppConfig.API_ROOT_URL + "/people",
        idAttribute: "id",

        initialize: function() {
            //this.on("change", this.change);
        },

        //Epoxy computeds -- these are computed automatically when other model values change
        computeds: {
            fullName: {
                deps: [ "firstName", "lastName" ],
                get: function (firstName, lastName) {
                    var fullName = $.trim(firstName + " " + $.trim(lastName));
                    return $.trim(fullName);
                },
                set: function(value) {
                    
                }
            }
            // fullName: function() {
            //     var fullName = $.trim(this.get("firstName") + " " + $.trim(this.get("lastName")));
            //     return $.trim(fullName);
            // }
        }

        // change: function() {
        //     this.set("firstName", $.trim(this.get("firstName")));
        //     this.set("lastName", $.trim(this.get("lastName")));
            
        //     this.set("fullName", $.trim(this.get("firstName") + " " + this.get("lastName")));
        // }
    });
    
    return personModel;
});
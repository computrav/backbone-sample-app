define([ "backbone", "./PersonModel", "text!./PersonTableRowTemplate.html" ],
function (Backbone, PersonModel, PersonTableRowTemplate) {
    var personTableRowView = Backbone.Epoxy.View.extend({
        initialize: function () {
            this.template = _.template(PersonTableRowTemplate);            
        },

        events: {
            "click": function(e) {
                this.trigger("PersonTableRow:clicked", e, this);

                //Here, we are using the Backbone object as an event bus (pub/sub model). We are triggering a specific event from this view,
                //and any other view(s) can "listen" for the event, and take action as they see fit. In other words, the events
                //are "broadcast" for others to pick up on
                //The event name here can be anything, but is named as below by convention so it's easy to tell where it came from
                //Backbone.trigger("customEvent");
            },
          
            "click .btnPersonDelete": function(e) {
                var self = this;
                
                //Remove the person and trigger an event
                this.model.destroy({
                    success: function() {
                        self.trigger("PersonTableRow:removed", e, self);
                    }
                });
              
                e.stopPropagation();
            }
        },

        render: function() {
            var html = this.template(this.model.toJSON());

            //Here, we are calling setElement() and passing in the HTML that was just created from the template above
            //This is useful in cases like this, where this view is an HTML fragment (in this case, a table row) which will
            //be appended to a parent container, by a parent view element (such as a table or table body)
            this.setElement(html);

            return this;
        }
    });
    
    return personTableRowView;
});
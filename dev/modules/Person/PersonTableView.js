define([ "backbone", "./PersonCollection", "text!./PersonTableTemplate.html", "./PersonTableRowView" ], 
function (Backbone, PersonCollection, PersonTableTemplate, PersonTableRowView) {
    var personTableView = Backbone.Epoxy.View.extend({
        initialize: function () {
            var self = this;

            this.template = _.template(PersonTableTemplate);
        },

        events: {
            "click": function(e) {

            }
        },

        render: function() {
            var html = this.template();
            this.$el.html(html);

            if (this.collection.length > 0) {
                this.$("#peopleTable").show();
                
                //Loop through items in the collection
                this.collection.each(function(personModel) {
                    var personTableRowView = new PersonTableRowView({
                        model: personModel
                    });

                    this.listenTo(personTableRowView, "PersonTableRow:clicked", function(e, view) {
                        this.trigger("PersonTable:rowClicked", e, view);
                    });

                    this.listenTo(personTableRowView, "PersonTableRow:removed", function(e, view) {
                        this.trigger("PersonTable:rowRemoved", e, view);
                    });

                    //Append HTML of person view (table row) to the collection view
                    this.$("table").append(personTableRowView.render().$el);
                }, this);
            } else {
                //No people found
                this.$("#noPeopleAlert").show();
            }

            return this;
        }
    });

    return personTableView;
});
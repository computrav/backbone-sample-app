define([ "backbone", "appConfig", "./PersonModel" ],
function (Backbone, AppConfig, PersonModel) {
    var personCollection = Backbone.Collection.extend({
        model: PersonModel,
        url: AppConfig.API_ROOT_URL + "/people"
    });
    
    return personCollection;
});
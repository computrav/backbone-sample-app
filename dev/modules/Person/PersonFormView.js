define([ "backbone", "backbone-epoxy", "util", "./PersonModel", "text!./PersonFormTemplate.html" ], 
function (Backbone, Epoxy, Util, PersonModel, PersonFormTemplate) {
    var personFormView = Backbone.Epoxy.View.extend({
        model: PersonModel,
        
        initialize: function () {
            this.template = _.template(PersonFormTemplate);
            this.render();
        },

        //Define Epoxy bindings
        bindings: {
            //Inputs
            "input#txtFirstName": "value:firstName,events:['keyup']",
            "input#txtLastName": "value:lastName,events:['keyup']",
            "input#txtFavoriteAnimal": "value:favoriteAnimal,events:['keyup']",

            //Static
            "#firstName": "text:firstName",
            "#lastName": "text:lastName",
            "#fullName": "text:fullName",
            "#favoriteAnimal": "text:favoriteAnimal"
        },

        events: {
            "click #btnUpdate": function() {
                var self = this;

                this.model.save(null, {
                    success: function() {
                        self.personChanged();
                    }
                });
            },

            "click #btnAdd": function() {
                var self = this;
              
                //Clear out the "id" field so the model appears new when doing save below
                this.model.unset("id");

                //Call save, which will do a POST to the server to add the record
                //Note here that to pass ALL model values, 'null' must be provided as the first param
                this.model.save(null, {
                    success: function() {
                        self.resetForm();
                        self.personChanged();
                        self.$("#addPersonSuccessAlert").show().delay(2000).slideUp(250);
                        //addPersonSuccessAlert
                    }
                });
            },

            "click #btnDelete": function() {
                var self = this;
              
                this.model.destroy({
                    success: function() {
                        self.personChanged();
                    }
                });
                this.render();
            },

            "click #btnAddNew": function(e) {
                e.target.blur();
                this.resetForm();
            }
        },
      
        personChanged: function() {
            //Trigger event indicating the form has been used to make a change to a person (add/update/delete)
            //Other views can listen for this event to take action as desired
            this.trigger("PersonFormView:personChanged");
        },
      
        resetForm: function() {
            Util.resetModel(this.model);
            this.render();
            this.$("#txtFirstName").focus();
        },
      
        initDom: function() {
            if (this.model.get("id") === "") {
                this.$("#introUpdate").hide();
                this.$("#introAdd").show();
                this.$("#addButton").show();
                this.$("#editButtons").hide();
            } else {
                this.$("#introUpdate").show();
                this.$("#introAdd").hide();
                this.$("#addButton").hide();
                this.$("#editButtons").show();
            }
        },

        render: function() {
            var html = this.template(this.model.toJSON());

            this.$el.html(html);
            this.applyBindings();
            this.initDom();

            return this;
        },

        hasFullName: function() {
            return $.trim(this.model.get("fullName")) !== "";
        }
    });
    
    return personFormView;
});
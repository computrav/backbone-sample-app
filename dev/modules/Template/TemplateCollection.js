define([ "backbone", "appConfig", "modules/Template/TemplateModel" ],
function (Backbone, AppConfig, TemplateModel) {
    var templateCollection = Backbone.Collection.extend({
        model: TemplateModel,
        url: AppConfig.API_ROOT_URL + "template
    });
    
    return templateCollection;
});
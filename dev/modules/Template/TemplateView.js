define([ "backbone", "modules/Template/TemplateModel", "text!modules/Template/TemplateTemplate.html" ],
function (Backbone, TemplateModel, TemplateTemplate) {
    var templateView = Backbone.View.extend({
        tagName: "div",

        initialize: function () {
            //Instantiate model with values
            this.model = new SampleModel({
                value1: "Banana",
                value2: "Television",
                value3: "Coffee"
            });
          
            //Load (compile) template
            this.template = _.template(SampleTemplate);
          
            //Render the view
            this.render();
        },

        //Define events for the view
        events: {
            "click": "onClick"  //When anything in the view is clicked, call the onClick() method (defined below)
        },

        //Event handler
        onClick: function() {
            //Do something when this event handler is called (i.e., when the event using this handler is fired)
            alert("Sample View was clicked!");
        },

        //Renders the view
        render: function() {
            //Pass JSON representation of model values into the template function to get HTML
            var html = this.template(this.model.toJSON());
          
            //Set the HTML to the DOM element associated with this view
            this.$el.html(html);
          
            //Return 'this' to support chaining
            return this;
        }
    });
    
    return templateView;
});
define([ "backbone", "appConfig" ],
function (Backbone, AppConfig) {
    //Define model with some defaults
    var templateModel = Backbone.Model.extend({
        defaults: {
            value1: "",
            value2: "",
            value3: ""
        },
        
        urlRoot: AppConfig.API_ROOT_URL + "/template",
        idAttribute: "id"
    });
    
    return templateModel;
});
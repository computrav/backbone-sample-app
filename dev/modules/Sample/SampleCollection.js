define([ "backbone", "appConfig", "modules/Sample/SampleModel" ],
function (Backbone, AppConfig, SampleModel) {
    var sampleCollection = Backbone.Collection.extend({
        model: SampleModel,
        url: AppConfig.API_ROOT_URL + "sample"
    });
    
    return sampleCollection;
});
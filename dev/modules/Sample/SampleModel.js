define([ "backbone", "appConfig" ],
function (Backbone, AppConfig) {
    //Define model with some defaults
    var sampleModel = Backbone.Model.extend({
        defaults: {
            address: "",
            city: "",
            state: "",
            zipCode: "",
            override: {}
        },
        urlRoot: AppConfig.API_ROOT_URL + "/sample",
        idAttribute: "id"
    });
    
    return sampleModel;
});
define([ "backbone", "appConfig" ],
function (Backbone, AppConfig) {
    //Define model with some defaults
    var sampleOverrideModel = Backbone.Model.extend({
        urlRoot: AppConfig.API_ROOT_URL + "/sample/complex",

        //Override the fetch method
        fetch: function(options) {
            var options = $.extend({}, {
                //Defaults
                doUnsuccessful: false  //Whether or not to simulate an unsuccessful call
            }, options);
            
            //This is checking a user-defined options parameter to do something special
            //In this case, set a custom URL
            if (options.doUnsuccessful === true) {
                this.url = AppConfig.API_ROOT_URL + "/sample/complex/error";
            }
            
            //this.constructor.__super__.fetch.apply(this, options);
            return Backbone.Model.prototype.fetch.call(this, options);
        },
      
        //Overriding the parse method to deal with API responses that may have a unique response structure
        //For example, instead of the model data being returned directly, the response also include other meta data and/or
        //the model data is 'nested' in a sub-element
        parse: function(response, options) {
            if (response.success === true) {
                return response.data;
            } else {
                console.log("parse(): response received, success=false -- Event object:");
                console.log(response.event);
              
                //Trigger error() callback
                options.error();

                return response;
            }
        },
      
        sync: function(method, model, options) {
            options = options || {};
            
            //Uncommenting below will issue a GET for "read" methods and POST for others (create, update, delete)
            //Backbone.emulateHTTP = true;
          
            //Example of adding some user-defined header to the request
            options.headers = {
                "X-Custom-Header": "abc123",
                "X-AUTH_TOKEN": "297cb281-fca1-4f8f-bee5-f28b6d47d52a"
            };

            //Other things you could override in sync()...
            //Suppose the API you're calling accepts a HTTP POST when doing an update (instead of the RESTful PUT that Backbone.js would issue)
            //This example will override the jQuery.ajax() option for the 'method' to be POST, and also sets a custom URL
            if (method == "update") {  //This would normally send a HTTP PUT request...
                options.method = "POST";
                options.url = AppConfig.API_ROOT_URL + "/sample/complex/update";
            }
          
            return Backbone.sync(method, model, options);
        }
    });
    
    return  sampleOverrideModel;
});
define([ "backbone", "./SampleModel", "./SampleOverrideModel", "text!./SampleTemplate.html" ],
function (Backbone, SampleModel, SampleOverrideModel, SampleTemplate) {
    var view = Backbone.View.extend({
        initialize: function () {
            var self = this;

            this.template = _.template(SampleTemplate);

            //Instantiate models
            this.model = new SampleModel({ id: 1 });
            this.overrideModel = new SampleOverrideModel({ id: 1 });

            //Do fetch() calls on the models
            var promise1 = this.model.fetch();

            var promise2 = this.overrideModel.fetch();

            //Do the following when BOTH fetch calls are complete
            $.when(promise1, promise2)
                .done(function(promise1, promise2) {
                    var overrideValues = {
                        address: self.overrideModel.get("address"),
                        city: self.overrideModel.get("city"),
                        state: self.overrideModel.get("state"),
                        zipCode: self.overrideModel.get("zipCode")
                    };

                    self.model.set("override", overrideValues);

                    self.render();
                })
                .fail(function(promise1, promise2) {
                    console.log("One or more of the model fetch() commands failed.");
                });
        },

        events: {
            
        },

        render: function() {
console.log(this.model.attributes);
            var html = this.template(this.model.toJSON());
            this.$el.html(html);
          
            return this;
        }
    });
    
    return view;
});
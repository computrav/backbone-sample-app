define([ "backbone", "text!./AppNavTemplate.html" ],
function (Backbone, AppNavTemplate) {
    var appNavView = Backbone.View.extend({
        initialize: function () {
            //Load (compile) template
            this.template = _.template(AppNavTemplate);
          
            //Render the view
            this.render();
        },

        //Define events for the view
        events: {
            
        },

        //Renders the view
        render: function() {
            //Pass JSON representation of model values into the template function to get HTML
            var html = this.template();
            
            //Set the HTML to the DOM element associated with this view
            this.$el.html(html);

            //Return 'this' to support chaining
            return this;
        }
    });
    
    return appNavView;
});
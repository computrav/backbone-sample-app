define([ "backbone", "util" ], function (Backbone, Util) {
    //Define application router
    var AppRouter = Backbone.Router.extend({
        routes: {
            "": "homeView",
            "home": "homeView",
            "model-overrides": "modelOverrides"

            //Other examples...
            // "person/:id": "getPerson",
            // "*actions": "defaultRoute"
        },

        homeView: function() {
            require([ "util", "modules/App/AppView" ], function(Util, AppView) {
                var appView = new AppView({
                    el: $("div#AppView")
                });

                Util.showView(appView);
            });
        },

        modelOverrides: function() {
            require([ "util", "modules/Sample/SampleView" ], function(Util, SampleView) {
                var sampleView = new SampleView({
                    el: $("div#SampleView")
                });

                Util.showView(sampleView);
            });
        }
    });

    return AppRouter;
});
//Backbone.js Utility Module - Misc utility methods
define([ "backbone", "modules/AppRouter" ], function (Backbone, AppRouter) {
    var Util = {
        currentView: null,  //Keeps track of the current view shown in the main part of the app (other views, such as nav, footer, may always be present)

        initRouter: function() {
            //Init the router
            var appRouter = new AppRouter;

            //Start Backbone history to support bookmarkable URLs
            Backbone.history.start();
        },

        //Sets element of view to selector (which moves over any bound events), then calls view's render() method
        renderView: function (view, selector) {
            view.setElement(selector);
            view.render();
        },

        showView: function(newView, options) {
            var options = $.extend({}, {
                //Option defaults
                trackView: true,  //Whether or not to track the view in the 'currentView'. If the view is meant to be more persistent on the page/app, this would generally be set to false
                doRender: false  //Whether or not the view's render() method should be called (typically, this is the case)
            }, options);


            //Close (remove) the current view if the view being requested is different
            if (options.trackView === true) {
                if (this.currentView !== null) {
                    if (this.currentView.cid != newView.cid) {
                        this.closeView(this.currentView);
                    }
                }

                this.currentView = newView;
            }

            //Render the desired view
            if (options.doRender === true) {
                newView.render();
            }
        },

        clearView: function(view) {
            //Stop listening for events
            view.undelegateEvents();

            //Remove all elements from the view's container
            view.$el.empty();
        },

        //Removes view and any child views (as defined by the 'childViews' element in the view)
        //Per Backbone.js docs:  remove() removes a view and its el from the DOM, and calls stopListening to remove any bound events that the view has listenTo'd.
        closeView: function(view) {
            var self = this;

            if (!_.isUndefined(view.childViews)) {
                _.each(view.childViews, function(childView) {
                    self.clearView(childView);
                });
            }

            this.clearView(view);
        },

        resetModel: function(model) {
            model.clear();

            if (!_.isUndefined(model.defaults)) {
                model.set(model.defaults);
            }
        }
    };

    return Util;
});
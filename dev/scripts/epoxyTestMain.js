//Define the paths to each of the modules we may need in our app (or that they may have as dependencies)
//The "key" of each entry is the short-hand name we can use within our app when calling RequireJS's require() and define() methods
require.config({
    paths: {
        "jquery":           "../vendor/jquery/dist/jquery.min",
        "underscore":       "../vendor/underscore-amd/underscore-min",
        "backbone":         "../vendor/backbone-amd/backbone-min",
        "backbone-epoxy":   "../vendor/backbone.epoxy/backbone.epoxy",
        "util":             "util"
    }
});

require([ "backbone", "backbone-epoxy" ], function(Backbone, Epoxy) {
    var bindModel = new Backbone.Model({
        firstName: "Luke",
        lastName: "Skywalker"
    });

    var BindingView = Backbone.Epoxy.View.extend({
        el: "#app-luke",

        bindings: {
            "input#txtFirstName": "value:firstName,events:['keyup']",
            "input#txtLastName": "value:lastName,events:['keyup']",
            "span#firstName": "text:firstName",
            "span#lastName": "text:lastName"
        }
    });

    var view = new BindingView({
        model: bindModel
    });
});
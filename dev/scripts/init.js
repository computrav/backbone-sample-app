//Define the paths to each of the modules we may need in our app (or that they may have as dependencies)
//The "key" of each entry is the short-hand name we can use within our app when calling RequireJS's require() and define() methods
require.config({
    baseUrl: "..",

    shim: {
        "bootstrap": {
            "deps": [ "jquery" ],
            "exports": "bootstrap"
        }
    },

    paths: {
        "jquery":           "vendor/jquery/dist/jquery.min",
        "underscore":       "vendor/underscore-amd/underscore-min",
        "backbone":         "vendor/backbone-amd/backbone-min",
        "backbone-epoxy":   "vendor/backbone.epoxy/backbone.epoxy",
        "text":             "vendor/text/text",
        "bootstrap":        "vendor/bootstrap/dist/js/bootstrap.min",
        "modules":          "modules",
        "util":             "scripts/backboneUtil",
        "appConfig":        "scripts/appConfig"
    }
});

//This initializes/starts our App
require([ "util", "modules/AppNav/AppNavView" ], function(Util, AppNavView) {
    Util.initRouter();

    var appNavView = new AppNavView({
        el: $("div#AppNavView")
    });

    Util.showView(appNavView, {
        trackView: false
    });
});

# Backbone.js Sample App #

## Prerequisites ##

* **Node.js** - Install from http://nodejs.org


## Setup Instructions ##

The app setup performs the following tasks:

- Installs all Node modules that are listed as dev dependencies in the `./package.json` file
- Runs `bower install`, which uses the Bower web package manager to install packages (defined in `bower.json`) into the target directory (defined in `.bowerrc`).

**To setup the app, run:**

        npm install


## Starting the App & Dev Environment ##

**To start the app, run:**

        npm start

**Here's what occurs when the app is started:**

- The Lite-Server starts (lightweight development web server)
- The JSON Server starts (super lightweight mock API server)
- The Chrome browser launches to the app's default web page (per `bs-config.json`)
- The browser automatically refreshes when files are changed (per `bs-config.json`)
- Typescript (.ts) files are transpiled to .js on the fly when they're changed (per `tsconfig.json`)

## Starting the API Server Only ##

**To only start the API server:**

        npm run api


## Directory Structure & Key Files ##

    /.vscode                          --> Visual Studio Code config folder
        launch.json                   --> Defines Debug configurations for VS Code Debugger
        settings.json                 --> User settings for VS Code
    /build                            --> Folder for build scripts
	    build.js                      --> r.js config file (combine/minify js, minify css, etc)
        build.sh                      --> shell script to run r.js, then remove files/dirs from /dist that aren't needed
    /dev                                           --> Main development folder
        /images                       --> Images used in the app
        /modules                      --> Main folder for application Modules (sub-folder for each module)
            /Person                   --> Sample folder for a "Person" module
                PersonModel.js        --> Backbone.js Model for the Person module
                PersonTemplate.html   --> Underscore template for the Person module (may be multiple)
                PersonView.js         --> Backbone.js View for the Person module
        /scripts                      --> Main scripts for the App, including 'main.js', which is ref'd by the Require.js <script> tag in index.html
        /vendor                       --> Bower target for installing web packages (per ./bowerrc)
    /dist                             --> Folder for build outputs (from running /build/build.sh)
    /node_modules                     --> Target for Node modules (created the first time 'npm install' is run)
    .bowerrc                          --> Bower config file for the web package target (i.e., /vendor)
    bower.json                        --> Bower config file defining web packages to install
    bs-config.json                    --> Browsersync config file
    index.html                        --> Main App index.html file
    package.json                      --> Npm Config file for the App
    README.md                         --> This file!
    tsconfig.json                     --> TypeScript config file
    tslint.json                       --> tslint config file


## Bower (Web Package Manager) ##

**There are two main files for Bower to be aware of:**

- **.bowerrc** - The main purpose of this file is to define the target directory for all web packages that are installed via Bower (in our case, ./vendor)
- **bower.json** - The main purpose of this file is to define the dependencies -- i.e., the Bower packages we want to install/update

### Installing and Updating Packages ###

**To install packages (as defined in `bower.json`):**

> **NOTE:** Packages are installed as part of `npm start` above, so this command does not normally need to be run separately.

        bower install

**To install a specific package:**

        bower install <packageName>

**To update packages (as defined in `bower.json`):**

        bower update


## Lite-Server / Browsersync ##

Lite-Server is used as the lightweight web server for hosting the application. 
The great thing about this is that it's built on top of Browsersync so that when files in your application change (i.e., as you're coding), your web browser will automatically refresh. This behavior is very customizable (and can even be disabled entirely if desired).

The main configuration file for Lite-Server is `./bs-config.json` (in the application root).

By default the web server binds to port 3000, so you can access your app at [http://localhost:3000](http://localhost:3000).

See more details about Lite-Server at the Git repo here: [https://github.com/johnpapa/lite-server](https://github.com/johnpapa/lite-server).


## JSON Server ##

JSON Server is a lightweight web server that is designed to host a mock RESTful API. You can easily define the mocked data by editing `./json-server-db.json`. Better yet, if you do POSTs and PUTs against the API, the database is updated! It also supports custom routing and custom modules if you need to extend functionality for things like access control or fancy routing.

The main configuration file for JSON Server is `./json-server.json` (in the application root).

By default, the JSON Server web server binds to port 2999, so you can access your app at [http://localhost:2999](http://localhost:2999). If you browse directly to this location, you can navigate to other endpoints that are built automatically based on the data defined in `json-server-db.json`.

Note that if you decide against using JSON Server locally, you can 

See more details about JSON Server at the Git repo here: [https://github.com/typicode/json-server](https://github.com/typicode/json-server).


## Require.js Notes ##

### Using require() vs define() ###

Use **require()** when you want to load a dependency (plus all of its own dependencies).

> **EXAMPLE:** Require that AppView and AppModel are loaded before this code is run

        require([ 'AppView', 'AppModel' ], function(view, model) {
            //Do something that requires AppView and AppModel
        });

Use **define()** when you want to define a module that will be reused by your app, specifying what other modules are needed.

> **EXAMPLE:** State that backbone is required if using this module

        define(["backbone"], function (Backbone) {
            var view = Backbone.View.extend({
                initialize: function () {
                    this.render();
                },

                render: function() {
                    this.$el.html("<div>Whatever</div>");
                    return this;
                }
            });

            return view;
        });


## Epoxy.js - 'Elegant Data Binding for Backbone' ##

By design, Backbone.js does not natively support binding between Models and Views/Templates. You can certainly define bindings manually, but it's a lot of work and would require careful eyes and maintenance.

Epoxy.js solves this issue by providing a robust library for one-way and two-way bindings, computeds, and much more. Epoxy.js offers Epoxy.Model and Epoxy.View (among other things) that are themselves extended from Backbone.Model and Backbone.View, so integrating Epoxy.js into your existing Backbone.js application can be done with specific models/views.

Learn more at [http://epoxyjs.org](http://epoxyjs.org).


## Visual Studio Code Debugging Setup ##

1. Create ./.vscode/launch.json, e.g.:

        {
            "version": "0.2.0",
            "configurations": [
                {
                    "name": "Launch Chrome",
                    "type": "chrome",
                    "request": "launch",
                    "url": "http://localhost:3000",
                    "webRoot": "${workspaceRoot}/dev",
                    "skipFiles": [ "node_modules", "vendor/*.*"],
                    "sourceMaps": true
                },
                {
                    "name": "Attach to Chrome",
                    "type": "chrome",
                    "request": "attach",
                    "port": 9222,
                    "webRoot": "${workspaceRoot}/dev",
                    "skipFiles": [ "node_modules", "vendor/*.*"],
                    "sourceMaps": true
                }
            ]
        }

1. From within VS Code, set breakpoints in your code
1. VS Code will break when breakpoints are hit, and the browser will 'pause'

### Notes ###

**Known Issue:** Opening Dev tools from within Chrome (F12) will crash/stop the attachment

## TODO ##

- "Re-brand" sample to be less-generic (e.g., soccer players instead of "people")
- Marionette integration?
- Unit Testing in Javascript (specifically, Backbone.js apps)
